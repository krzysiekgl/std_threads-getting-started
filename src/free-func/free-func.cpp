/*
 * Author: KrzysiekGL - webmaster@unexpectd.com
 *
 * std::thread getting started
 * based on the instructions from site:
 * https://riptutorial.com/cplusplus/example/2330/creating-a-std--thread
 *
 * Free function example
 */

#include <iostream>
#include <thread>
#include <atomic>

int foo(std::atomic<int> * atom) {
	return atom->fetch_add(1, std::memory_order_seq_cst);
}

int main(const int argc, const char ** argv, const char ** eval) {
	// Just a variable to play with
	std::atomic<int> a;
	a.store(10, std::memory_order_seq_cst);
	std::cout << "a is eq to: " << a.load(std::memory_order_seq_cst) << std::endl;

	// Threads - free function example
	std::thread thread0(foo, &a);
	std::thread thread1(foo, &a);
	std::thread thread2(foo, &a);
	std::thread thread3(foo, &a);

	// Barrier to wait for all of them finish
	thread0.join();
	thread1.join();
	thread2.join();
	thread3.join();

	std::cout << "a is eq to: " << a.load(std::memory_order_seq_cst) << std::endl;

	return 0;
}
