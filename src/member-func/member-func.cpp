/*
 * Author: KrzysiekGL - webmaster@unexpectd.com
 *
 * std::thread getting started
 * based on the instructions from site:
 * https://riptutorial.com/cplusplus/example/2330/creating-a-std--thread
 *
 * Member function example
 */

#include <iostream>
#include <thread>

class Bar {
	public:
		void foo(int a) {
			std::cout << a << std::endl;
		}
};

int main(const int argc, const char ** argv, const char ** eval) {
	Bar bar;

	// Create and execute a thread
	std::thread thread(&Bar::foo, &bar, 10); // Pass 10 to member funcion

	// The member function will be executed on a separate thread
	//
	// Wait for the thread to finish, this is a blocking operation
	thread.join();

	std::cout << "Hello form the main thread of execution!\n";

	return 0;
}
