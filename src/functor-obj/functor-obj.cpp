/*
 * Author: KrzysiekGL - webmaster@unexpectd.com
 *
 * std::thread getting started
 * based on the instructions from site:
 * https://riptutorial.com/cplusplus/example/2330/creating-a-std--thread
 *
 *
 * Functor object example
 */

#include <iostream>
#include <thread>

class Bar {
	public:
		void operator()(int a) {
			std::cout << a << std::endl;
		}
};

int main(const int argc, const char ** argv, const char ** eval) {
	Bar bar;
	
	// Create and execute a thread
	std::thread thread(bar, 10); // Pass 10 to functor object

	// The functor object will be executed in a separate thread
	//
	// Wait for the thread to finish, this is a blocking operation
	thread.join();

	std::cout << "Hello from the main execution thread!\n";

	return 0;
}
