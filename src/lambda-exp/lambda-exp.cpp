/*
 * Author: KrzysiekGL - webmaster@unexpectd.com
 *
 * std::thread getting started
 * based on the instructions from site:
 * https://riptutorial.com/cplusplus/example/2330/creating-a-std--thread
 *
 * Lambda expression example
 */

#include <iostream>
#include <thread>

int main(const int argc, const char ** argv, const char ** eval) {
	auto lambda = [](int a) { std::cout << a << std::endl; };

	// Create and execute a thread
	std::thread thread(lambda, 44); // Pass 44 to the lambda expression

	// The lambda expression will be executed on a separate thread
	//
	// Wait for the thread to finish, this is a blocking opeartion
	thread.join();

	std::cout << "Hello from the main thread here!\n";

	return 0;
}
